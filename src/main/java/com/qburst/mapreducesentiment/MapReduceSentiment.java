package com.qburst.mapreducesentiment;

import com.qburst.mapreducesentiment.helper.AggregateCount;
import com.qburst.mapreducesentiment.helper.CustomHbaseFilter;
import com.qburst.mapreducesentiment.helper.DBWriter;
import com.qburst.sentimentanalyser.SentimentAnalyser;
import com.qburst.sentimentanalyser.models.SAResult;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.NullOutputFormat;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

public class MapReduceSentiment {

    //private Logger logger = Log;

    private static final byte[] DATA_COLUMN_FAMILY = "data".getBytes();
    private static final byte[] METADATA_COLUMN_FAMILY = "metadata".getBytes();

    private static final byte[] DATA_HEADER = "data".getBytes();
    private static final byte[] CELEBRITY_HEADER = "celebrity".getBytes();

    static Date startDate;


    private static DBWriter db = new DBWriter("107.178.219.43", 5866, "ZTNiMGM0NDI5OGZjMWMxNDlhZmJmNGM4OTk2ZmI5");

    public MapReduceSentiment() {

    }

    static class SentimentMapper extends TableMapper<String, Double> {

        public SentimentMapper() {
        }

        @Override
        protected void map(ImmutableBytesWritable rowkey, Result columns, Context context) throws IOException, InterruptedException

        {
            byte[] data = columns.getValue(DATA_COLUMN_FAMILY, DATA_HEADER);
            String celebrity = columns.getValue(METADATA_COLUMN_FAMILY, CELEBRITY_HEADER).toString();
            SentimentAnalyser sentimentAnalyser = new SentimentAnalyser();
            SAResult result = sentimentAnalyser.analyseText(data.toString());
            context.write(celebrity, new Double(result.getSentiments()));
        }
    }

    static class SentimentCombiner extends Reducer<String, Double, String, AggregateCount<Double>> {
        public SentimentCombiner() {
        }

        @Override
        protected void reduce(String key, Iterable<Double> values, Context context) throws IOException, InterruptedException

        {
            // Add up all of the page views for this hour
            double sum = 0;
            int count = 0;
            for (Double value : values) {
                sum += value;
                count++;
            }
            // Write out the current hour and the sum
            context.write(key, new AggregateCount<Double>(sum, count));
        }
    }

    static class SentimentReducer extends Reducer<String, AggregateCount<Double>, String, NullWritable> {
        public SentimentReducer() {
        }

        @Override
        protected void reduce(String key, Iterable<AggregateCount<Double>> values, Context context) throws IOException, InterruptedException

        {
            // Add up all of the page views for this hour
            double sum = 0;
            long count = 0;
            for (AggregateCount<Double> value : values) {
                sum += value.getValue();
                count += value.getCount();
            }
            // Write out the current hour and the sum
            db.addSentiment(key, startDate, sum);
            context.write(key, NullWritable.get());
        }
    }

    public static void main(String[] args) {
        try {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -1);
            startDate = cal.getTime();

            cal.add(Calendar.DATE, -1);
            Date endDate = cal.getTime();

            Configuration conf = HBaseConfiguration.create();
            Job job = new Job(conf, "Sentiment Analysis");
            job.setJarByClass(MapReduceSentiment.class);

            // Create a scan
            Scan scan = new Scan();
            //CustomHbaseFilter hbaseFilter = new CustomHbaseFilter(new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()));
            //scan.setFilter(hbaseFilter);
            scan.setCaching(500);
            scan.setCacheBlocks(false);
            // Configure the Map process to use HBase
            TableMapReduceUtil.initTableMapperJob(
                    "structured_data",                    // The name of the table
                    scan,                           // The scan to execute against the table
                    SentimentMapper.class,                 // The Mapper class
                    String.class,             // The Mapper output key class
                    Double.class,             // The Mapper output value class
                    job);                          // The Hadoop job

            // Configure the reducer process
            job.setCombinerClass(SentimentCombiner.class);
            job.setReducerClass(SentimentReducer.class);
            // Setup the output - we'll write to the file system: HOUR_OF_DAY   PAGE_VIEW_COUNT
            job.setOutputKeyClass(String.class);
            job.setOutputValueClass(NullWritable.class);

            job.setOutputFormatClass(NullOutputFormat.class);

            // We'll run just one reduce task, but we could run multiple
            job.setNumReduceTasks(1);

            // Write the results to a file in the output directory
            FileOutputFormat.setOutputPath(job, new Path("output"));

            FileOutputFormat.setOutputPath(job, new Path("/user/sample/sentiment/output"));
            // We'll run just one reduce task, but we could run multiple
            job.setNumReduceTasks(1);
            // Execute the job
            boolean completion = job.waitForCompletion(true);
            if (completion == true) {

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}