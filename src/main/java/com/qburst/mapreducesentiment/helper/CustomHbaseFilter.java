package com.qburst.mapreducesentiment.helper;


import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.filter.FilterBase;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Arrays;

public class CustomHbaseFilter extends FilterBase {

    private Timestamp start, end;
    static final byte[] DATA_HEADER = "data".getBytes();
    static final byte[] CELEBRITY_HEADER = "celebrity".getBytes();

    public CustomHbaseFilter(Timestamp start, Timestamp end) {
        super();
        this.start = start;
        this.end = end;
    }

    @Override
    public boolean filterRowKey(byte[] bytes, int i, int i1) throws IOException {
        StringBuffer s = new StringBuffer(bytes.toString());
        Timestamp key = new Timestamp(Long.valueOf(s.reverse().toString()));
        if (key.after(start) && key.before(end)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean filterAllRemaining() throws IOException {
        return false;
    }

    @Override
    public ReturnCode filterKeyValue(Cell cell) throws IOException {
        byte[] qualifier = cell.getQualifierArray();
        if (Arrays.equals(qualifier, DATA_HEADER) ||Arrays.equals(qualifier,CELEBRITY_HEADER)) {
            return ReturnCode.INCLUDE;
        } else {
            return ReturnCode.SKIP;
        }
    }
}
