package com.qburst.mapreducesentiment.helper;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DBWriter {
    private static JedisPool pool;

    public DBWriter(String host, int port, String password) {
        pool = new JedisPool(new JedisPoolConfig(), host, port, 2000, password);
    }

    public void addSentiment(String celebrity, Date date, double sentiment) {
        try (Jedis jedis = pool.getResource()) {
            jedis.hset(celebrity, new SimpleDateFormat("dd-MM-yyyy").format(date), String.valueOf(sentiment));
        }

    }
}
