package com.qburst.mapreducesentiment.helper;


public class AggregateCount<Type> {
    private Type value ;
    private long  count ;

    public AggregateCount(Type value , long count){
        this.value = value;
        this.count = count;
    }

    public Type getValue() {
        return value;
    }

    public void setValue(Type value) {
        this.value = value;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
