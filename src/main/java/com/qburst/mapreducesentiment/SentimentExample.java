package com.qburst.mapreducesentiment;


import com.qburst.sentimentanalyser.SentimentAnalyser;
import com.qburst.sentimentanalyser.models.SAResult;

public class SentimentExample {

    public static void main(String arg[]) {

        SentimentAnalyser sentimentAnalyser = new SentimentAnalyser();
        SAResult result = sentimentAnalyser.analyseText("Sachin is not playing for india today.Today will be a magic day for india");
        System.out.println("Text : " + result.getText());
        System.out.println("Categories : " + result.getCategory());
        System.out.println("Sentiments : " + result.getSentiments());
    }
}
